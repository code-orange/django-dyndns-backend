class DynDnsRouter:
    """
    A router to control all database operations on models in the
    django_voipstack application.
    """

    def db_for_read(self, model, **hints):
        if model._meta.app_label == "django_powerdns_models":
            return "pdns"
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == "django_powerdns_models":
            return "pdns"
        return None

    def allow_relation(self, obj1, obj2, **hints):
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return None
